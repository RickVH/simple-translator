<?php

namespace Domain\Shared\Persister;

interface FlushablePersister
{
    public function persist(object $model): void;

    public function flush(): void;
}