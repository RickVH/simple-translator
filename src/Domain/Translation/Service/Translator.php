<?php

namespace Domain\Translation\Service;

use Domain\Translation\Collection\LanguageCode as LanguageCodeCollection;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\ValueObject\LanguageCode;

interface Translator
{
    public function translate(string $text, LanguageCode $inputLanguage, LanguageCodeCollection $targetLanguages): TranslationResult;
}