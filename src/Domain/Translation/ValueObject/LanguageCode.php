<?php
declare(strict_types=1);

namespace Domain\Translation\ValueObject;

use Domain\Translation\Exception\ValueObject\InvalidLanguageCode;

final class LanguageCode
{
    public const CZECH = 'cs';
    public const DANISH = 'da';
    public const DUTCH = 'nl';
    public const ENGLISH = 'en';
    public const FINNISH = 'fi';
    public const FRENCH = 'fr';
    public const GERMAN = 'de';
    public const GREEK = 'el';
    public const HUNGARIAN = 'hu';
    public const ITALIAN = 'it';
    public const NORWEGIAN = 'no';
    public const POLISH = 'pl';
    public const PORTUGUESE = 'pt';
    public const ROMANIAN = 'ro';
    public const SLOVAK = 'sk';
    public const SPANISH = 'es';
    public const SWEDISH = 'sv';
    public const TURKISH = 'tr';

    public const VALID_CODES = [
        self::CZECH,
        self::DANISH,
        self::DUTCH,
        self::ENGLISH,
        self::FINNISH,
        self::FRENCH,
        self::GERMAN,
        self::GREEK,
        self::HUNGARIAN,
        self::ITALIAN,
        self::NORWEGIAN,
        self::POLISH,
        self::PORTUGUESE,
        self::ROMANIAN,
        self::SLOVAK,
        self::SPANISH,
        self::SWEDISH,
        self::TURKISH,
    ];

    private string $value;

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @throws InvalidLanguageCode
     */
    public static function create(string $value): self
    {
        if (!in_array($value, self::VALID_CODES)) {
            throw new InvalidLanguageCode($value);
        }

        return new self($value);
    }

    public static function createCzech(): self
    {
        return new self(self::CZECH);
    }

    public static function createDanish(): self
    {
        return new self(self::DANISH);
    }

    public static function createDutch(): self
    {
        return new self(self::DUTCH);
    }

    public static function createEnglish(): self
    {
        return new self(self::ENGLISH);
    }

    public static function createFinnish(): self
    {
        return new self(self::FINNISH);
    }

    public static function createFrench(): self
    {
        return new self(self::FRENCH);
    }

    public static function createGerman(): self
    {
        return new self(self::GERMAN);
    }

    public static function createGreek(): self
    {
        return new self(self::GREEK);
    }

    public static function createHungarian(): self
    {
        return new self(self::HUNGARIAN);
    }

    public static function createItalian(): self
    {
        return new self(self::ITALIAN);
    }

    public static function createNorwegian(): self
    {
        return new self(self::NORWEGIAN);
    }

    public static function createPolish(): self
    {
        return new self(self::POLISH);
    }

    public static function createPortuguese(): self
    {
        return new self(self::PORTUGUESE);
    }

    public static function createRomanian(): self
    {
        return new self(self::ROMANIAN);
    }

    public static function createSlovak(): self
    {
        return new self(self::SLOVAK);
    }

    public static function createSpanish(): self
    {
        return new self(self::SPANISH);
    }

    public static function createSwedish(): self
    {
        return new self(self::SWEDISH);
    }

    public static function createTurkish(): self
    {
        return new self(self::TURKISH);
    }

    public function value(): string
    {
        return $this->value;
    }
}
