<?php
declare(strict_types=1);

namespace Domain\Translation\ValueObject;

final class Translation
{
    private LanguageCode $language;
    private string $value;

    public function __construct(LanguageCode $language, string $value)
    {
        $this->language = $language;
        $this->value = $value;
    }

    public function language(): LanguageCode
    {
        return $this->language;
    }

    public function value(): string
    {
        return $this->value;
    }
}