<?php
declare(strict_types=1);

namespace Domain\Translation\Command;

final class Translate
{
    private string $text;
    private string $sourceLanguageCode;
    /** @var string[] */
    private array $targetLanguageCodes;

    public function __construct(string $text, string $sourceLanguage, array $targetLanguageCodes)
    {
        $this->text = $text;
        $this->sourceLanguageCode = $sourceLanguage;
        $this->targetLanguageCodes = [];

        foreach ($targetLanguageCodes as $targetLanguageCode) {
            $this->addTargetLanguage($targetLanguageCode);
        }
    }

    private function addTargetLanguage(string $languageCode): void
    {
        $this->targetLanguageCodes[] = $languageCode;
    }

    public function text(): string
    {
        return $this->text;
    }

    public function sourceLanguageCode(): string
    {
        return $this->sourceLanguageCode;
    }

    /**
     * @return string[]
     */
    public function targetLanguageCodes(): array
    {
        return $this->targetLanguageCodes;
    }
}