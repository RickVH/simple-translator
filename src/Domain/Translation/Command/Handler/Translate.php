<?php
declare(strict_types=1);

namespace Domain\Translation\Command\Handler;

use Domain\Shared\Persister\FlushablePersister;
use Domain\Translation\Collection\LanguageCode as LanguageCodeCollection;
use Domain\Translation\Command\Translate as TranslateCommand;
use Domain\Translation\Exception\Service\Translator\TranslatorException;
use Domain\Translation\Exception\ValueObject\InvalidLanguageCode;
use Domain\Translation\Service\Translator;
use Domain\Translation\ValueObject\LanguageCode;

final class Translate
{
    private Translator $translator;
    private FlushablePersister $persister;

    public function __construct(Translator $translator, FlushablePersister $persister)
    {
        $this->translator = $translator;
        $this->persister = $persister;
    }

    /**
     * @throws InvalidLanguageCode|TranslatorException
     */
    public function handle(TranslateCommand $command): void
    {
        $translationResult = $this->translator->translate(
            $command->text(),
            LanguageCode::create($command->sourceLanguageCode()),
            self::createTargetTranslationCollection($command->targetLanguageCodes())
        );

        $this->persister->persist($translationResult);
        $this->persister->flush();
    }

    /**
     * @param string[] $targetLanguagesCodes
     * @throws InvalidLanguageCode
     */
    private static function createTargetTranslationCollection(array $targetLanguagesCodes): LanguageCodeCollection
    {
        $languageCodes = [];
        foreach ($targetLanguagesCodes as $targetLanguagesCode) {
            $languageCodes[] = LanguageCode::create($targetLanguagesCode);
        }

        return new LanguageCodeCollection($languageCodes);
    }
}