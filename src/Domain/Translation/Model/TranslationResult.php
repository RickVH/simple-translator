<?php
declare(strict_types=1);

namespace Domain\Translation\Model;

use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\ValueObject\LanguageCode;

final class TranslationResult
{
    private string $inputValue;
    private LanguageCode $inputLanguage;
    private TranslationCollection $translations;

    public function __construct(string $inputValue, LanguageCode $inputLanguage, TranslationCollection $translations)
    {
        $this->inputValue = $inputValue;
        $this->inputLanguage = $inputLanguage;
        $this->translations = $translations;
    }

    public function inputValue(): string
    {
        return $this->inputValue;
    }

    public function inputLanguage(): LanguageCode
    {
        return $this->inputLanguage;
    }

    public function translations(): TranslationCollection
    {
        return $this->translations;
    }
}