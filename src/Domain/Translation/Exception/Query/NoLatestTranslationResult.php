<?php
declare(strict_types=1);

namespace Domain\Translation\Exception\Query;

use Exception;
use Throwable;

final class NoLatestTranslationResult extends Exception
{
    public function __construct(int $code = 0, Throwable $previous = null)
    {
        parent::__construct('No latest translation result was found', $code, $previous);
    }
}