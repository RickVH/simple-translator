<?php
declare(strict_types=1);

namespace Domain\Translation\Exception\ValueObject;

use Exception;
use Throwable;

final class InvalidLanguageCode extends Exception
{
    private string $invalidCode;

    public function __construct(string $invalidCode, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Invalid language code "%s" is given', $invalidCode), $code, $previous);
        $this->invalidCode = $invalidCode;
    }

    public function invalidCode(): string
    {
        return $this->invalidCode;
    }
}