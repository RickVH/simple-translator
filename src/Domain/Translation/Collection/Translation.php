<?php
declare(strict_types=1);

namespace Domain\Translation\Collection;

use Domain\Translation\ValueObject\LanguageCode;
use Domain\Translation\ValueObject\Translation as TranslationValueObject;
use Iterator;
use function current;
use function key;
use function ksort;
use function next;
use function reset;

final class Translation implements Iterator
{
    /** @var TranslationValueObject[] */
    private array $translations;

    /**
     * @param TranslationValueObject[] $translations
     */
    public function __construct(array $translations = [])
    {
        $this->translations = [];

        foreach ($translations as $translation) {
            $this->add($translation);
        }

        ksort($this->translations);
    }

    private function add(TranslationValueObject $translation): void
    {
        $this->translations[$translation->language()->value()] = $translation;
    }

    public function byLanguage(LanguageCode $languageCode): ?TranslationValueObject
    {
        return $this->translations[$languageCode->value()] ?? null;
    }

    public function current(): TranslationValueObject|false
    {
        return current($this->translations);
    }

    public function next(): TranslationValueObject|false
    {
        return next($this->translations);
    }

    public function key(): string|null
    {
        return key($this->translations);
    }

    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind(): void
    {
        reset($this->translations);
    }
}