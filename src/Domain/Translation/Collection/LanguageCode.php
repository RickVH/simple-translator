<?php
declare(strict_types=1);

namespace Domain\Translation\Collection;

use Domain\Translation\ValueObject\LanguageCode as LanguageCodeValueObject;
use Iterator;
use function current;
use function key;
use function next;
use function reset;

final class LanguageCode implements Iterator
{
    /** @var LanguageCodeValueObject[] */
    private array $languageCodes;

    /**
     * @param LanguageCodeValueObject[] $languageCodes
     */
    public function __construct(array $languageCodes = [])
    {
        $this->languageCodes = [];

        foreach ($languageCodes as $languageCode) {
            $this->add($languageCode);
        }

        ksort($this->languageCodes);
    }

    private function add(LanguageCodeValueObject $languageCode): void
    {
        $this->languageCodes[$languageCode->value()] = $languageCode;
    }

    public function current(): LanguageCodeValueObject|false
    {
        return current($this->languageCodes);
    }

    public function next(): LanguageCodeValueObject|false
    {
        return next($this->languageCodes);
    }

    public function key(): string|null
    {
        return key($this->languageCodes);
    }

    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind(): void
    {
        reset($this->languageCodes);
    }
}
