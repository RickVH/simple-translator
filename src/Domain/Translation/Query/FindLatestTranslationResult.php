<?php

namespace Domain\Translation\Query;

use Domain\Translation\Exception\Query\NoLatestTranslationResult;
use Domain\Translation\Model\TranslationResult;

interface FindLatestTranslationResult
{
    /**
     * @throws NoLatestTranslationResult
     */
    public function query(): TranslationResult;
}