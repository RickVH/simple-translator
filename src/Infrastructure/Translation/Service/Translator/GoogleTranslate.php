<?php
declare(strict_types=1);

namespace Infrastructure\Translation\Service\Translator;

use Domain\Translation\Collection\LanguageCode as LanguageCodeCollection;
use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\Service\Translator;
use Domain\Translation\ValueObject\LanguageCode;
use Domain\Translation\ValueObject\Translation;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils as PromiseUtils;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Utils;
use function urlencode;

final class GoogleTranslate implements Translator
{
    private const FETCH_URL = '/translate_a/single?client=gtx&sl=%s&tl=%s&dt=t&q=%s';
    private const LABEL_UNABLE_TO_TRANSLATE = 'Unable to translate';

    private Client $guzzleClient;

    public function __construct(Client $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    public function translate(string $text, LanguageCode $inputLanguage, LanguageCodeCollection $targetLanguages): TranslationResult
    {
        $requestResults = PromiseUtils::settle(self::contractPromises($this->guzzleClient, $text, $inputLanguage, $targetLanguages))
            ->wait();

        $translations = [];
        foreach ($requestResults as $languageCode => $requestResult) {
            if ($requestResult['state'] !== 'fulfilled') {
                $translations[] = new Translation(LanguageCode::create($languageCode), self::LABEL_UNABLE_TO_TRANSLATE);
                continue;
            }

            /** @var Response $response */
            $response = $requestResult['value'];
            if ($response->getBody() === null) {
                $translations[] = new Translation(LanguageCode::create($languageCode), self::LABEL_UNABLE_TO_TRANSLATE);
                continue;
            }

            $decodedResponseContent = Utils::jsonDecode($response->getBody()->getContents());

            $translations[] = new Translation(LanguageCode::create($languageCode), $decodedResponseContent[0][0][0]);
        }

        return new TranslationResult($text, $inputLanguage, new TranslationCollection($translations));
    }

    private static function contractPromises(
        Client $client,
        string $text,
        LanguageCode $sourceLanguage,
        LanguageCodeCollection $targetLanguages
    ): array
    {
        $promises = [];
        foreach ($targetLanguages as $targetLanguage) {
            $targetLanguageCode = $targetLanguage->value();
            $promises[$targetLanguageCode] = $client->getAsync(sprintf(
                self::FETCH_URL,
                $sourceLanguage->value(),
                $targetLanguageCode,
                urlencode($text)
            ));
        }

        return $promises;
    }
}