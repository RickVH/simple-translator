<?php
declare(strict_types=1);

namespace Infrastructure\Translation\Repository\Memory;

use Domain\Translation\Model\TranslationResult as TranslationResultModel;

final class TranslationResult
{
    private ?TranslationResultModel $translationResult;

    public function __construct()
    {
        $this->translationResult = null;
    }

    public function setLatest(TranslationResultModel $translationResult): void
    {
        $this->translationResult = $translationResult;
    }

    public function findLatest(): ?TranslationResultModel
    {
        return $this->translationResult;
    }
}