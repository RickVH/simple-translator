<?php

namespace Infrastructure\Translation\Exception\Persister\ModelPersister;

use Exception;
use Throwable;

final class InvalidPersistModel extends Exception
{
    private object $invalidModel;
    private string $expectedModelClassName;

    public function __construct(
        object $invalidModel,
        string $expectedModelClassName,
        int $code = 0,
        Throwable $previous = null
    )
    {
        parent::__construct(
            sprintf('Received model "%s", expected ""%s', $invalidModel::class, $expectedModelClassName),
            $code,
            $previous
        );

        $this->invalidModel = $invalidModel;
        $this->expectedModelClassName = $expectedModelClassName;
    }

    public function invalidModel(): object
    {
        return $this->invalidModel;
    }

    public function expectedModelClassName(): string
    {
        return $this->expectedModelClassName;
    }
}