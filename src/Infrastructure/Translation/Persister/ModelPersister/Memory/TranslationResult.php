<?php
declare(strict_types=1);

namespace Infrastructure\Translation\Persister\ModelPersister\Memory;

use Domain\Translation\Model\TranslationResult as TranslationResultModel;
use Infrastructure\Shared\Persister\ModelPersister\Memory as MemoryModelPersister;
use Infrastructure\Translation\Exception\Persister\ModelPersister\InvalidPersistModel;
use Infrastructure\Translation\Repository\Memory\TranslationResult as MemoryTranslationResultRepository;

final class TranslationResult implements MemoryModelPersister
{
    private MemoryTranslationResultRepository $repository;

    public function __construct(MemoryTranslationResultRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getModelClassName(): string
    {
        return TranslationResultModel::class;
    }

    public function persist(object $model): void
    {
        if (!$model instanceof TranslationResultModel) {
            throw new InvalidPersistModel($model, $this->getModelClassName());
        }

        $this->repository->setLatest($model);
    }
}