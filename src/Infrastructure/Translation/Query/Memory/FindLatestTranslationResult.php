<?php
declare(strict_types=1);

namespace Infrastructure\Translation\Query\Memory;

use Domain\Translation\Exception\Query\NoLatestTranslationResult;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\Query\FindLatestTranslationResult as FindLatestTranslationResultInterface;
use Infrastructure\Translation\Repository\Memory\TranslationResult as MemoryTranslationResultRepository;

final class FindLatestTranslationResult implements FindLatestTranslationResultInterface
{
    private MemoryTranslationResultRepository $translationResultRepository;

    public function __construct(MemoryTranslationResultRepository $translationResultRepository)
    {
        $this->translationResultRepository = $translationResultRepository;
    }

    public function query(): TranslationResult
    {
        $latestTranslationResult = $this->translationResultRepository->findLatest();

        if ($latestTranslationResult === null) {
            throw new NoLatestTranslationResult();
        }

        return $latestTranslationResult;
    }
}