<?php
declare(strict_types=1);

namespace Infrastructure\Shared\Persister\FlushablePersister;

use Domain\Shared\Persister\FlushablePersister;
use Infrastructure\Shared\Persister\ModelPersister\Memory as MemoryPersister;
use RuntimeException;
use function array_key_exists;

final class Memory implements FlushablePersister
{
    /** @var object[] */
    private array $modelsToPersist;
    /** @var MemoryPersister[] */
    private array $persisters;

    public function __construct()
    {
        $this->modelsToPersist = [];
        $this->persisters = [];
    }

    public function addPersister(MemoryPersister $persister)
    {
        $this->persisters[$persister->getModelClassName()] = $persister;
    }

    public function persist(object $model): void
    {
        if (!$this->hasPersister($model)) {
            throw new RuntimeException(sprintf('No persister available for model "%s"', $model::class));
        }

        $this->modelsToPersist[] = $model;
    }

    public function flush(): void
    {
        foreach ($this->modelsToPersist as $model) {
            $this->persisters[$model::class]->persist($model);
        }

        $this->modelsToPersist = [];
    }

    private function hasPersister(object $model): bool
    {
        return array_key_exists($model::class, $this->persisters);
    }
}