<?php

namespace Infrastructure\Shared\Persister\ModelPersister;

use Infrastructure\Shared\Persister\ModelPersister;

interface Memory extends ModelPersister
{
}