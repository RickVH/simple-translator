<?php
declare(strict_types=1);

namespace Infrastructure\Shared\Persister;

use Infrastructure\Translation\Exception\Persister\ModelPersister\InvalidPersistModel;

interface ModelPersister
{
    public function getModelClassName(): string;

    /**
     * @throws InvalidPersistModel
     */
    public function persist(object $model): void;
}