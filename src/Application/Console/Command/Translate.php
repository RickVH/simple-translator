<?php
declare(strict_types=1);

namespace Application\Console\Command;

use Domain\Translation\Command\Handler\Translate as TranslateCommandHandler;
use Domain\Translation\Command\Translate as TranslateCommand;
use Domain\Translation\Exception\Query\NoLatestTranslationResult;
use Domain\Translation\Exception\ValueObject\InvalidLanguageCode;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\Query\FindLatestTranslationResult as FindLatestTranslationResultQuery;
use Domain\Translation\ValueObject\LanguageCode;
use Domain\Translation\ValueObject\Translation;
use LogicException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\ConsoleSectionOutput;
use Symfony\Component\Console\Output\OutputInterface;
use function array_diff;
use function array_map;
use function count;
use function implode;

final class Translate extends Command
{
    private TranslateCommandHandler $translateCommandHandler;
    private FindLatestTranslationResultQuery $findLatestTranslationResultQuery;

    public function __construct(
        TranslateCommandHandler $translateCommandHandler,
        FindLatestTranslationResultQuery $findLatestTranslationResultQuery,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->translateCommandHandler = $translateCommandHandler;
        $this->findLatestTranslationResultQuery = $findLatestTranslationResultQuery;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Translates text to other languages')
            ->addArgument('text', InputArgument::REQUIRED, 'The text to translate')
            ->addOption('source-language', 's', InputOption::VALUE_REQUIRED, 'The language code of the text to translate', 'en')
            ->addOption('translation-language', 't', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED, 'The language code to translate to')
            ->addOption('all-translation-languages', 'a', InputOption::VALUE_NONE, 'With this option the text is translated to all available languages');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$output instanceof ConsoleOutputInterface) {
            throw new LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }

        $headerSection = $output->section();
        $this->outputHeader($headerSection);

        $contentSection = $output->section();

        [$text, $sourceLanguageCode, $translationLanguageCodes, $translateToAllLanguages] = $this->getInputs($input);

        try {
            $languagesToTranslateTo = $this->validateTranslationLanguageCodes($translationLanguageCodes, $sourceLanguageCode, $translateToAllLanguages);
            $this->outputFetchingStatus($contentSection, $text, $sourceLanguageCode, $languagesToTranslateTo);

            $this->translateCommandHandler->handle(new TranslateCommand($text, $sourceLanguageCode, $languagesToTranslateTo));

            $translationResult = $this->findLatestTranslationResultQuery->query();

            $this->outputTranslationResult($contentSection, $translationResult);

            return Command::SUCCESS;
        } catch (InvalidArgumentException | NoLatestTranslationResult | InvalidLanguageCode $exception) {
            $contentSection->clear();
            $contentSection->writeln($exception->getMessage());

            return Command::FAILURE;
        }
    }

    private function getInputs(InputInterface $input): array
    {
        return [
            $input->getArgument('text'),
            $input->getOption('source-language'),
            $input->getOption('translation-language'),
            $input->getOption('all-translation-languages'),
        ];
    }

    /**
     * @param string[] $translationLanguageCodes
     */
    private function validateTranslationLanguageCodes(array $translationLanguageCodes, string $sourceLanguageCode, bool $translateToAllLanguages): array
    {
        if ($translateToAllLanguages) {
            return array_diff(LanguageCode::VALID_CODES, [$sourceLanguageCode]);
        }

        if (count($translationLanguageCodes) === 0) {
            throw new InvalidArgumentException('At least one translation-language needs to be specified.');
        }

        return array_diff($translationLanguageCodes, [$sourceLanguageCode]);
    }

    private function outputHeader(ConsoleSectionOutput $section): void
    {
        $section->writeln([
            'Simple translator',
            '=================',
            ''
        ]);
    }

    /**
     * @param string[] $translationLanguageCodes
     */
    private function outputFetchingStatus(
        ConsoleSectionOutput $section,
        string $text,
        string $sourceLanguageCode,
        array $translationLanguageCodes
    ): void
    {
        $section->writeln(sprintf(
            'Translating "%s" from "%s" to %s ...',
            $text,
            $sourceLanguageCode,
            implode(', ', array_map(static function (string $translationLanguageCode) {
                return sprintf('"%s"', $translationLanguageCode);
            }, $translationLanguageCodes))
        ));
    }

    private function outputTranslationResult(ConsoleSectionOutput $contentSection, TranslationResult $translationResult): void
    {
        $contentSection->clear();
        $contentSection->writeln([
            sprintf('Translations for "%s" (%s):', $translationResult->inputValue(), $translationResult->inputLanguage()->value()),
            '',
        ]);

        /** @var Translation $translation */
        foreach ($translationResult->translations() as $translation) {
            $contentSection->writeln([
                sprintf('%s: %s', $translation->language()->value(), $translation->value())
            ]);
        }

        $contentSection->writeln([
            '',
            '================='
        ]);
    }
}