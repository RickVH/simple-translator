<?php
declare(strict_types=1);

use Application\Console\Command\Translate as TranslateConsoleCommand;
use Domain\Shared\Persister\FlushablePersister;
use Domain\Translation\Command\Handler\Translate as TranslateCommandHandler;
use Domain\Translation\Query\FindLatestTranslationResult as FindLatestTranslationResultQuery;
use Domain\Translation\Service\Translator;
use GuzzleHttp\Client as GuzzleClient;
use Infrastructure\Shared\Persister\FlushablePersister\Memory as MemoryFlushablePersister;
use Infrastructure\Translation\Persister\ModelPersister\Memory\TranslationResult as MemoryTranslationResultPersister;
use Infrastructure\Translation\Query\Memory\FindLatestTranslationResult as MemoryFindLatestTranslationResultQuery;
use Infrastructure\Translation\Repository\Memory\TranslationResult as MemoryTranslationResultRepository;
use Infrastructure\Translation\Service\Translator\GoogleTranslate as GoogleTranslateTranslator;
use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Loader\Configurator\ReferenceConfigurator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $configurator) {
    $services = $configurator->services();
    $services
        ->defaults()
        ->autowire()
        ->autoconfigure();

    $services->set(TranslateCommandHandler::class);

    //// Application

    $services->set(Application::class)
        ->args(['Simple Translator', '1.0'])
        ->call('add', [new ReferenceConfigurator(TranslateConsoleCommand::class)])
        ->public();

    $services->set(TranslateConsoleCommand::class)
        ->arg('$name', 'translate');

    //// Domain

    // Persisters
    $services->alias(FlushablePersister::class, MemoryFlushablePersister::class);

    // Query
    $services->alias(FindLatestTranslationResultQuery::class, MemoryFindLatestTranslationResultQuery::class);

    // Services
    $services->alias(Translator::class, GoogleTranslateTranslator::class);

    //// Infrastructure

    // Persisters
    $services->set(MemoryFlushablePersister::class)
        ->call('addPersister', [service(MemoryTranslationResultPersister::class)]);
    $services->set(MemoryTranslationResultPersister::class);

    // Query
    $services->set(MemoryFindLatestTranslationResultQuery::class);

    // Repositories
    $services->set(MemoryTranslationResultRepository::class);

    // Services
    $services->set(GoogleTranslateTranslator::class, GoogleTranslateTranslator::class)
        ->arg('$guzzleClient', service('guzzle_client.google_translate'));

    $services->set('guzzle_client.google_translate', GuzzleClient::class)
        ->arg('$config', ['base_uri' => 'https://translate.googleapis.com/']);
};

