# Simple Translator

This is a CLI command that uses, in its current state, Google Translate to translate a given text into different languages.

## Installation

Clone the repository and use Composer to install the required libraries. Basic example:

```bash
git clone https://gitlab.com/RickVH/simple-translator.git
cd simple-translator
composer install --no-dev
```

**Note:** There is also a Docker image available, see below.

## Usage

### Translate a sentence to other language

To translate a sentence, a translation language option (--translation-language or -t) needs to be specified.

```bash
php bin/console translate --translation-language de "Translating is hard"

Simple translator
=================

Translations for "Translating is hard" (en):

de: Übersetzen ist schwer

=================
```

### Translate sentence to multiple languages

Multiple translation language options can be specified to retrieve multiple translations.

```bash
php bin/console translate --translation-language de --translation-language es "Translating is hard"

Simple translator
=================

Translations for "Translating is hard" (en):

de: Übersetzen ist schwer
es: Traducir es difícil

=================
```

### Translate sentence to all available languages

To translate the given text to all available languages, the all translation option (--all-translation-languages or -a)
can be specified.

```bash
php bin/console translate --all-translation-languages "Translating is hard"

Simple translator
=================

Translations for "Translating is hard" (en):

cs: Překlad je těžký
da: Det er svært at oversætte
de: Übersetzen ist schwer
el: Η μετάφραση είναι δύσκολη
es: Traducir es difícil
fi: Kääntäminen on vaikeaa
fr: Traduire est difficile
hu: A fordítás nehéz
it: Tradurre è difficile
nl: Vertalen is moeilijk
no: Å oversette er vanskelig
pl: Tłumaczenie jest trudne
pt: Traduzir é dificil
ro: Traducerea este grea
sl: Prevajanje je težko
sv: Att översätta är svårt
tr: Çeviri yapmak zordur

=================
```

### Specify input language

English is the default input language. This can be specified with the source language option (--source-language or -s).

```bash
php bin/console translate --source-language es --translation-language en --translation-language de "Traducir es difícil"

Simple translator
=================

Translations for "Traducir es difícil" (es):

de: Übersetzen ist schwierig
en: Translating is difficult

=================
```

## Docker

The command can also be run through Docker. By doing this, 
the previously mentioned installation steps are no longer needed.

```bash
docker run --rm -ti rickvhoeij/simple-translator:latest translate --translation-language de "Translating is hard"

Simple translator
=================

Translations for "Translating is hard" (en):

de: Übersetzen ist schwer

=================
``` 

## Available language codes

- **cs**: CZECH
- **da**: DANISH
- **nl**: DUTCH
- **en**: ENGLISH
- **fi**: FINNISH
- **fr**: FRENCH
- **de**: GERMAN
- **el**: GREEK
- **hu**: HUNGARIAN
- **it**: ITALIAN
- **no**: NORWEGIAN
- **pl**: POLISH
- **pt**: PORTUGUESE
- **ro**: ROMANIAN
- **sk**: SLOVAK
- **es**: SPANISH
- **sv**: SWEDISH
- **tr**: TURKISH

## License
[MIT](https://choosealicense.com/licenses/mit/)
