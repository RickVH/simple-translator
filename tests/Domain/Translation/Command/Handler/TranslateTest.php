<?php
declare(strict_types=1);

namespace Test\Domain\Translation\Command\Handler;

use Domain\Shared\Persister\FlushablePersister;
use Domain\Translation\Collection\LanguageCode as LanguageCodeCollection;
use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\Command\Handler\Translate as TranslateHandler;
use Domain\Translation\Command\Translate as TranslateCommand;
use Domain\Translation\Exception\ValueObject\InvalidLanguageCode;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\Service\Translator;
use Domain\Translation\ValueObject\LanguageCode;
use Domain\Translation\ValueObject\Translation;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class TranslateTest extends TestCase
{
    private Translator|MockObject $mockTranslator;
    private FlushablePersister|MockObject $mockPersister;
    private TranslateHandler $handler;

    protected function setUp(): void
    {
        $this->mockTranslator = $this->createMock(Translator::class);
        $this->mockPersister = $this->createMock(FlushablePersister::class);
        $this->handler = new TranslateHandler($this->mockTranslator, $this->mockPersister);
    }

    /**
     * @test
     */
    public function itCanTranslate(): void
    {
        $this->mockTranslator
            ->expects(self::once())
            ->method('translate')
            ->with(
                'translating is hard',
                LanguageCode::createEnglish(),
                new LanguageCodeCollection([LanguageCode::createGerman()])
            )
            ->willReturn(new TranslationResult(
                'translating is hard',
                LanguageCode::createEnglish(),
                new TranslationCollection([
                    new Translation(LanguageCode::createGerman(), 'Übersetzen ist schwer')
                ])
            ));

        $this->mockPersister
            ->expects(self::once())
            ->method('persist')
            ->with(self::callback(static function (TranslationResult $translationResult) {
                self::assertSame('translating is hard', $translationResult->inputValue());
                self::assertSame('en', $translationResult->inputLanguage()->value());
                $germanTranslation = $translationResult->translations()->byLanguage(LanguageCode::createGerman());
                self::assertInstanceOf(Translation::class, $germanTranslation);
                self::assertSame('de', $germanTranslation->language()->value());
                self::assertSame('Übersetzen ist schwer', $germanTranslation->value());

                return true;
            }));

        $this->mockPersister
            ->expects(self::once())
            ->method('flush');

        $command = new TranslateCommand('translating is hard', 'en', ['de']);
        $this->handler->handle($command);
    }

    /**
     * @test
     */
    public function itThrowsExceptionWithInvalidSourceLanguageCode(): void
    {
        $command = new TranslateCommand('translating is hard', 'invalid-code', ['de']);

        $this->expectExceptionObject(new InvalidLanguageCode('invalid-code'));

        $this->handler->handle($command);
    }

    /**
     * @test
     */
    public function itThrowsExceptionWithInvalidTargetLanguageCode(): void
    {
        $command = new TranslateCommand('translating is hard', 'en', ['invalid-code']);

        $this->expectExceptionObject(new InvalidLanguageCode('invalid-code'));

        $this->handler->handle($command);
    }
}