<?php
declare(strict_types=1);

namespace Test\Domain\Translation\ValueObject;

use Domain\Translation\Exception\ValueObject\InvalidLanguageCode;
use Domain\Translation\ValueObject\LanguageCode;
use Generator;
use PHPUnit\Framework\TestCase;

final class LanguageCodeTest extends TestCase
{
    /**
     * @test
     * @dataProvider validLanguageCodeProvider
     */
    public function itCanCreateLanguageCode(string $validCode): void
    {
        $languageCode = LanguageCode::create($validCode);
        self::assertSame($validCode, $languageCode->value());
    }

    public function validLanguageCodeProvider(): Generator
    {
        yield [LanguageCode::ENGLISH];
        yield [LanguageCode::GERMAN];
    }

    /**
     * @test
     */
    public function itCanCreateThoughNameConstructor(): void
    {
        $namedConstructed = [
            LanguageCode::CZECH => LanguageCode::createCzech(),
            LanguageCode::DANISH => LanguageCode::createDanish(),
            LanguageCode::DUTCH => LanguageCode::createDutch(),
            LanguageCode::ENGLISH => LanguageCode::createEnglish(),
            LanguageCode::FINNISH => LanguageCode::createFinnish(),
            LanguageCode::FRENCH => LanguageCode::createFrench(),
            LanguageCode::GERMAN => LanguageCode::createGerman(),
            LanguageCode::GREEK => LanguageCode::createGreek(),
            LanguageCode::HUNGARIAN => LanguageCode::createHungarian(),
            LanguageCode::ITALIAN => LanguageCode::createItalian(),
            LanguageCode::NORWEGIAN => LanguageCode::createNorwegian(),
            LanguageCode::POLISH => LanguageCode::createPolish(),
            LanguageCode::PORTUGUESE => LanguageCode::createPortuguese(),
            LanguageCode::ROMANIAN => LanguageCode::createRomanian(),
            LanguageCode::SLOVAK => LanguageCode::createSlovak(),
            LanguageCode::SPANISH => LanguageCode::createSpanish(),
            LanguageCode::SWEDISH => LanguageCode::createSwedish(),
            LanguageCode::TURKISH => LanguageCode::createTurkish(),
        ];

        foreach ($namedConstructed as $expectedLanguageCode => $languageCodeObject) {
            self::assertSame($expectedLanguageCode, $languageCodeObject->value());
        }
    }

    /**
     * @test
     */
    public function itThrowsExceptionOnInvalidLanguageCode(): void
    {
        $invalidLanguageCode = 'invalid-code';

        $this->expectExceptionObject(new InvalidLanguageCode($invalidLanguageCode));

        LanguageCode::create($invalidLanguageCode);
    }


}
