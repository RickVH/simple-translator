<?php
declare(strict_types=1);

namespace Test\Domain\Translation\Collection;

use Domain\Translation\Collection\LanguageCode as LanguageCodeCollection;
use Domain\Translation\ValueObject\LanguageCode;
use PHPUnit\Framework\TestCase;

final class LanguageCodeTest extends TestCase
{
    /**
     * @test
     */
    public function itCanIterateThroughCollection(): void
    {
        $collection = new LanguageCodeCollection([
            LanguageCode::createEnglish(),
            LanguageCode::createGerman(),
        ]);

        $first = true;
        foreach ($collection as $languageCode) {
            if ($first) {
                self::assertSame('de', $languageCode->value());
                $first = false;
                continue;
            }

            self::assertSame('en', $languageCode->value());
        }
    }
}