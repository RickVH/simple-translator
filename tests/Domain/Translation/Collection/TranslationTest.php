<?php
declare(strict_types=1);

namespace Test\Domain\Translation\Collection;

use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\ValueObject\LanguageCode;
use Domain\Translation\ValueObject\Translation;
use PHPUnit\Framework\TestCase;

final class TranslationTest extends TestCase
{
    /**
     * @test
     */
    public function itCanGetTranslationByLanguage(): void
    {
        $englishLanguageCode = LanguageCode::createEnglish();
        $collection = new TranslationCollection([
            new Translation($englishLanguageCode, 'translating is hard')
        ]);

        self::assertSame('translating is hard', $collection->byLanguage($englishLanguageCode)->value());
    }

    /**
     * @test
     */
    public function itCanIterateThroughCollection(): void
    {
        $collection = new TranslationCollection([
            new Translation(LanguageCode::createEnglish(), 'translating is hard'),
            new Translation(LanguageCode::createGerman(), 'Übersetzen ist schwer'),
        ]);

        $first = true;
        foreach ($collection as $translation) {
            if ($first) {
                self::assertSame('de', $translation->language()->value());
                self::assertSame('Übersetzen ist schwer', $translation->value());

                $first = false;
                continue;
            }

            self::assertSame('en', $translation->language()->value());
            self::assertSame('translating is hard', $translation->value());
        }
    }


    /**
     * @test
     */
    public function itReturnsNullIfNoTranslationIsPresentWithGivenLanguage(): void
    {
        $collection = new TranslationCollection();
        self::assertNull($collection->byLanguage(LanguageCode::createEnglish()));
    }
}