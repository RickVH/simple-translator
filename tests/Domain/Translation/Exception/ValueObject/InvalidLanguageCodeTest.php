<?php
declare(strict_types=1);

namespace Test\Domain\Translation\Exception\ValueObject;

use Domain\Translation\Exception\ValueObject\InvalidLanguageCode;
use PHPUnit\Framework\TestCase;

final class InvalidLanguageCodeTest extends TestCase
{
    /**
     * @test
     */
    public function itContainsInvalidCode(): void
    {
        $invalidCode = 'invalid-code';

        $exception = new InvalidLanguageCode($invalidCode);

        self::assertSame($exception->invalidCode(), $invalidCode);
    }
}