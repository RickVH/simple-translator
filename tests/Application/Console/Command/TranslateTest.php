<?php

namespace Test\Application\Console\Command;

use Application\Console\Command\Translate;
use Domain\Shared\Persister\FlushablePersister;
use Domain\Translation\Collection\LanguageCode as LanguageCodeCollection;
use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\Command\Handler\Translate as TranslateCommandHandler;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\Query\FindLatestTranslationResult;
use Domain\Translation\Service\Translator;
use Domain\Translation\ValueObject\LanguageCode;
use Domain\Translation\ValueObject\Translation;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\ConsoleSectionOutput;
use function array_merge;
use function end;
use function is_array;
use function is_string;

final class TranslateTest extends TestCase
{
    private MockObject|Translator $mockTranslator;
    private MockObject|FlushablePersister $mockPersister;
    private MockObject|FindLatestTranslationResult $mockQuery;
    private MockObject|ConsoleSectionOutput $mockOutput;
    /** @var string[] */
    private array $outputLines;
    private Translate $command;

    protected function setUp(): void
    {
        $this->mockTranslator = $this->createMock(Translator::class);
        $this->mockPersister = $this->createMock(FlushablePersister::class);
        $this->mockQuery = $this->createMock(FindLatestTranslationResult::class);

        $this->outputLines = [];
        $this->mockOutput = $this->createMock(ConsoleOutputInterface::class);
        $mockSection = $this->createMock(ConsoleSectionOutput::class);
        $this->mockOutput
            ->expects(self::exactly(2))
            ->method('section')
            ->willReturn($mockSection);

        $mockSection
            ->expects(self::atLeastOnce())
            ->method('writeln')
            ->willReturnCallback(function(array|string $lines) {
                if (is_array($lines)) {
                    $this->outputLines = array_merge($this->outputLines, $lines);
                }
                else if (is_string($lines)) {
                    $this->outputLines[] = $lines;
                }
            });

        $handler = new TranslateCommandHandler($this->mockTranslator, $this->mockPersister);
        $this->command = new Translate($handler, $this->mockQuery);
    }

    /**
     * @test
     */
    public function itReturnsErrorWhenNoTranslationLanguageCodeIsGiven(): void
    {
        $input = new ArrayInput([
            'text' => 'Translating is hard'
        ]);

        $this->command->run($input, $this->mockOutput);

        self::assertSame('At least one translation-language needs to be specified.', end($this->outputLines));
    }

    /**
     * @test
     */
    public function itCanTranslate(): void
    {
        $translationResult = new TranslationResult(
            'Translating is hard',
            LanguageCode::createEnglish(),
            new TranslationCollection([
                new Translation(LanguageCode::createGerman(), 'Übersetzen ist schwer')
            ])
        );

        $this->mockTranslator
            ->expects(self::once())
            ->method('translate')
            ->with('Translating is hard', LanguageCode::createEnglish(), new LanguageCodeCollection([LanguageCode::createGerman()]))
            ->willReturn($translationResult);

        $this->mockQuery
            ->expects(self::once())
            ->method('query')
            ->willReturn($translationResult);

        $input = new ArrayInput([
            'text' => 'Translating is hard',
            '-t' => ['de']
        ]);

        $this->command->run($input, $this->mockOutput);

        self::assertContains('de: Übersetzen ist schwer', $this->outputLines);
    }

    /**
     * @test
     */
    public function itCanTranslateAllLanguages(): void
    {
        $translationResult = new TranslationResult(
            'Translating is hard',
            LanguageCode::createEnglish(),
            new TranslationCollection([
                new Translation(LanguageCode::createGerman(), 'Übersetzen ist schwer')
            ])
        );

        $this->mockTranslator
            ->expects(self::once())
            ->method('translate')
            ->with('Translating is hard', LanguageCode::createEnglish(), new LanguageCodeCollection([
                LanguageCode::createCzech(),
                LanguageCode::createDanish(),
                LanguageCode::createDutch(),
                LanguageCode::createFinnish(),
                LanguageCode::createFrench(),
                LanguageCode::createGerman(),
                LanguageCode::createGreek(),
                LanguageCode::createHungarian(),
                LanguageCode::createItalian(),
                LanguageCode::createNorwegian(),
                LanguageCode::createPolish(),
                LanguageCode::createPortuguese(),
                LanguageCode::createRomanian(),
                LanguageCode::createSlovak(),
                LanguageCode::createSpanish(),
                LanguageCode::createSwedish(),
                LanguageCode::createTurkish()
            ]))
            ->willReturn($translationResult);

        $this->mockQuery
            ->expects(self::once())
            ->method('query')
            ->willReturn($translationResult);

        $input = new ArrayInput([
            'text' => 'Translating is hard',
            '-a' => true
        ]);

        $this->command->run($input, $this->mockOutput);

        self::assertContains('de: Übersetzen ist schwer', $this->outputLines);
    }
}
