<?php

namespace Test\Infrastructure\Translation\Repository\Memory;

use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\ValueObject\LanguageCode;
use Infrastructure\Translation\Repository\Memory\TranslationResult as MemoryTranslationResultRepository;
use PHPUnit\Framework\TestCase;

final class TranslationResultTest extends TestCase
{
    private MemoryTranslationResultRepository $repository;

    protected function setUp(): void
    {
        $this->repository = new MemoryTranslationResultRepository();
    }

    /**
     * @test
     */
    public function itReturnsNullWhenNoResultIsSet(): void
    {
        self::assertNull($this->repository->findLatest());
    }

    /**
     * @test
     */
    public function itCanSetAndFindLatestResult(): void
    {
        $translationResult = new TranslationResult(
            'Test',
            LanguageCode::createEnglish(),
            new TranslationCollection()
        );

        $this->repository->setLatest($translationResult);

        self::assertSame($translationResult, $this->repository->findLatest());
    }


}