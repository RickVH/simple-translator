<?php

namespace Test\Infrastructure\Translation\Persister\ModelPersister\Memory;

use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\ValueObject\LanguageCode;
use Infrastructure\Translation\Exception\Persister\ModelPersister\InvalidPersistModel;
use Infrastructure\Translation\Persister\ModelPersister\Memory\TranslationResult as MemoryTranslationResultPersister;
use Infrastructure\Translation\Repository\Memory\TranslationResult as MemoryTranslationResultRepository;
use PHPUnit\Framework\TestCase;
use stdClass;

final class TranslationResultTest extends TestCase
{
    private MemoryTranslationResultRepository $translationResultRepository;
    private MemoryTranslationResultPersister $persister;

    protected function setUp(): void
    {
        $this->translationResultRepository = new MemoryTranslationResultRepository();
        $this->persister = new MemoryTranslationResultPersister($this->translationResultRepository);
    }

    /**
     * @test
     */
    public function itCanPersistModel(): void
    {
        $translationResult = new TranslationResult(
            'Test',
            LanguageCode::createEnglish(),
            new TranslationCollection()
        );

        $this->persister->persist($translationResult);

        self::assertSame($translationResult, $this->translationResultRepository->findLatest());
    }

    /**
     * @test
     */
    public function itThrowsExceptionWhenInvalidModelGiven(): void
    {
        $this->expectException(InvalidPersistModel::class);

        $this->persister->persist(new stdClass());
    }

    /**
     * @test
     */
    public function itCanReturnSupportedModelClass(): void
    {
        self::assertSame(TranslationResult::class, $this->persister->getModelClassName());
    }
}