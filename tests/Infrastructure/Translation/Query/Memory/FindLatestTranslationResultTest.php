<?php

namespace Test\Infrastructure\Translation\Query\Memory;

use Domain\Translation\Collection\Translation as TranslationCollection;
use Domain\Translation\Exception\Query\NoLatestTranslationResult;
use Domain\Translation\Model\TranslationResult;
use Domain\Translation\ValueObject\LanguageCode;
use Infrastructure\Translation\Query\Memory\FindLatestTranslationResult as MemoryFindLatestTranslationResultQuery;
use Infrastructure\Translation\Repository\Memory\TranslationResult as MemoryTranslationResultRepository;
use PHPUnit\Framework\TestCase;

final class FindLatestTranslationResultTest extends TestCase
{
    private MemoryTranslationResultRepository $translationRepository;
    private MemoryFindLatestTranslationResultQuery $query;

    protected function setUp(): void
    {
        $this->translationRepository = new MemoryTranslationResultRepository();
        $this->query = new MemoryFindLatestTranslationResultQuery($this->translationRepository);
    }

    /**
     * @test
     */
    public function itCanQueryTheLatestTranslationResult(): void
    {
        $translationResult = new TranslationResult(
            'Test',
            LanguageCode::createEnglish(),
            new TranslationCollection()
        );

        $this->translationRepository->setLatest($translationResult);

        self::assertSame($translationResult, $this->query->query());
    }

    /**
     * @test
     */
    public function itThrowsExceptionWhenNoLatestResultIsFound(): void
    {
        $this->expectException(NoLatestTranslationResult::class);

        $this->query->query();
    }


}