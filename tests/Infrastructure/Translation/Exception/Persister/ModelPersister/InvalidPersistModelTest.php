<?php

namespace Test\Infrastructure\Translation\Exception\Persister\ModelPersister;

use Infrastructure\Translation\Exception\Persister\ModelPersister\InvalidPersistModel;
use PHPUnit\Framework\TestCase;
use stdClass;

final class InvalidPersistModelTest extends TestCase
{
    private InvalidPersistModel $exception;

    protected function setUp(): void
    {
        $this->exception = new InvalidPersistModel(
            new stdClass(),
            'ValidClass'
        );
    }

    /**
     * @test
     */
    public function itOffersInvalidModelGetter(): void
    {
        self::assertInstanceOf(stdClass::class, $this->exception->invalidModel());
    }

    /**
     * @test
     */
    public function itOffersExpectedModelClassnameGetter(): void
    {
        self::assertSame('ValidClass', $this->exception->expectedModelClassName());
    }
}