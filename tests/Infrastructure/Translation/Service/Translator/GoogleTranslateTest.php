<?php
declare(strict_types=1);

namespace Test\Infrastructure\Translation\Service\Translator;

use Domain\Translation\Collection\LanguageCode as LanguageCodeCollection;
use Domain\Translation\ValueObject\LanguageCode;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Infrastructure\Translation\Service\Translator\GoogleTranslate;
use PHPUnit\Framework\TestCase;

final class GoogleTranslateTest extends TestCase
{
    private GoogleTranslate $translator;
    private MockHandler $mockHandler;

    protected function setUp(): void
    {
        $this->mockHandler = new MockHandler();
        $guzzleClient = new Client([
            'base_uri' => 'https://translate.googleapis.com/',
            'handler' => HandlerStack::create($this->mockHandler)
        ]);
        $this->translator = new GoogleTranslate($guzzleClient);
    }

    /**
     * @test
     */
    public function itCanTranslate(): void
    {
        $this->mockHandler->append(
            new Response(200, [], $this->createSuccessfulResponseContent('translating is hard', 'Übersetzen ist schwer'))
        );

        $translationResult = $this->translator->translate(
            'translating is hard',
            LanguageCode::createEnglish(),
            new LanguageCodeCollection([
                LanguageCode::createGerman()
            ])
        );

        self::assertSame('translating is hard', $translationResult->inputValue());
        self::assertSame('en', $translationResult->inputLanguage()->value());
        self::assertSame('de', $translationResult->translations()->byLanguage(LanguageCode::createGerman())->language()->value());
        self::assertSame('Übersetzen ist schwer', $translationResult->translations()->byLanguage(LanguageCode::createGerman())->value());
    }

    /**
     * @test
     */
    public function itReturnUnableToTranslateOnNon200Response(): void
    {
        $this->mockHandler->append(
            new Response(500)
        );

        $translationResult = $this->translator->translate(
            'translating is hard',
            LanguageCode::createEnglish(),
            new LanguageCodeCollection([
                LanguageCode::createGerman()
            ])
        );

        self::assertSame('translating is hard', $translationResult->inputValue());
        self::assertSame('en', $translationResult->inputLanguage()->value());
        self::assertSame('de', $translationResult->translations()->byLanguage(LanguageCode::createGerman())->language()->value());
        self::assertSame('Unable to translate', $translationResult->translations()->byLanguage(LanguageCode::createGerman())->value());
    }

    /**
     * @test
     */
    public function itReturnUnableToTranslateOnEmptyBody(): void
    {
        $mockResponse = $this->createMock(Response::class);

        $this->mockHandler->append(
            $mockResponse
        );

        $translationResult = $this->translator->translate(
            'translating is hard',
            LanguageCode::createEnglish(),
            new LanguageCodeCollection([
                LanguageCode::createGerman()
            ])
        );

        self::assertSame('translating is hard', $translationResult->inputValue());
        self::assertSame('en', $translationResult->inputLanguage()->value());
        self::assertSame('de', $translationResult->translations()->byLanguage(LanguageCode::createGerman())->language()->value());
        self::assertSame('Unable to translate', $translationResult->translations()->byLanguage(LanguageCode::createGerman())->value());
    }

    private function createSuccessfulResponseContent(string $inputText, string $translatedText): string
    {
        return sprintf(
            '[[["%s","%s",null,null,3,null,null,[[]],[[["4d5e201a34474a12730540dc3bfae9fd","tea_en_de_2020q1.md"]]]]],null,"en",null,null,null,null,[]]',
            $translatedText,
            $inputText
        );
    }
}