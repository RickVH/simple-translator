<?php
declare(strict_types=1);

namespace Test\Infrastructure\Shared\Persister\FlushablePersister;

use Infrastructure\Shared\Persister\FlushablePersister\Memory as MemoryFlushablePersister;
use Infrastructure\Shared\Persister\ModelPersister\Memory as MemoryModelPersisterInterface;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use stdClass;

final class MemoryTest extends TestCase
{
    /**
     * @test
     */
    public function itCanPersistAndFlush(): void
    {
        $flushablePersister = new MemoryFlushablePersister();

        $object = new stdClass();
        $mockPersister = $this->createMock(MemoryModelPersisterInterface::class);

        $mockPersister
            ->expects(self::once())
            ->method('getModelClassName')
            ->willReturn(stdClass::class);

        $mockPersister
            ->expects(self::once())
            ->method('persist')
            ->with($object);

        $flushablePersister->addPersister($mockPersister);
        $flushablePersister->persist($object);
        $flushablePersister->flush();
    }

    /**
     * @test
     */
    public function itThrowsExceptionWhenNoPersisterAvailable(): void
    {
        $flushablePersister = new MemoryFlushablePersister();

        $this->expectException(RuntimeException::class);

        $flushablePersister->persist(new stdClass());
    }


}